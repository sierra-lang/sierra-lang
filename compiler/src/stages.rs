// Compilation Stages
#[derive(Debug, PartialEq, Eq)]
pub enum Stage {
    // Process tokens in input
    Tokenize,
    // Parse tokens into AST
    Parse,
    // Typecheck AST
    Typecheck,
    // Generate code
    Codegen,
    // Create the output binary
    Link,
}

impl Stage {
    pub fn from_str(s: &str) -> Result<Stage, String> {
        match s {
            "tokenize" | "t" => Ok(Stage::Tokenize),
            "parse" | "p" => Ok(Stage::Parse),
            "typecheck" | "tc" => Ok(Stage::Typecheck),
            "codegen" | "cg" => Ok(Stage::Codegen),
            "link" | "l" => Ok(Stage::Link),

            _ => Err(format!("Unknown stage: {}", s)),
        }
    }

    fn next(&self) -> Option<Stage> {
        match self {
            Stage::Tokenize => Some(Stage::Parse),
            Stage::Parse => Some(Stage::Typecheck),
            Stage::Typecheck => Some(Stage::Codegen),
            Stage::Codegen => Some(Stage::Link),
            Stage::Link => None,
        }
    }

    // Get a list of all stages up to the given stage
    pub fn stages_to(final_stage: Stage) -> Vec<Stage> {
        let final_stage = Some(final_stage);
        let mut stages = Vec::new();
        let mut current_stage = Some(Stage::Tokenize);
        while current_stage != final_stage {
            match current_stage {
                Some(stage) => {
                    current_stage = stage.next();
                    stages.push(stage);
                }
                None => break,
            }
        }
        stages.push(final_stage.unwrap());
        stages
    }
}
