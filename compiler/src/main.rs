mod cli;
mod stages;

use structopt::StructOpt;

use {cli::CLI, stages::Stage};

fn main() {
    let args = CLI::from_args();

    let stages_to_run = Stage::stages_to(args.stage);

    println!("{:?}", stages_to_run);
}
