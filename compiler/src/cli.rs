use {
    std::path::PathBuf,
    structopt::{self, StructOpt},
};

use crate::stages::Stage;

#[derive(StructOpt, Debug)]
pub struct CLI {
    #[structopt(
        short = "v",
        long = "verbose",
        help = "Print verbose output",
        parse(from_occurrences)
    )]
    pub verbose: usize,

    #[structopt(help = "The input files to process", required = true)]
    pub input_files: Vec<PathBuf>,

    #[structopt(
        help = "The output file to write to",
        short = "o",
        long = "output",
        default_value = "a.out"
    )]
    pub output_file: PathBuf,

    #[structopt(
        help="the compilation stage to run to", 
        short="s", long="stage", 
        parse(try_from_str = Stage::from_str),
        default_value="link",
    )]
    pub stage: Stage,

    #[structopt(
        help = "The stages to save intermediate files for",
        long = "save-stages",
        parse(try_from_str = Stage::from_str),
        default_value="link",
    )]
    pub stage_save: Vec<Stage>,
}
